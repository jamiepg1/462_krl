
ruleset lab2Ruleset {
	meta {
		name "cs 462 ruleset 3"
		description <<
			Written for cs462 w2014
		>>
		author "braveSirRobin"
		logging off
	}
	dispatch {
	}
	global {
	}

	rule clearName {
		select when pageview ".*"
		pre {
			findClear = function(str) {
					(str.match(re#^.*?clear=([^&]+).*#)) => str.replace(re#^.*?clear=([^&]+).*#,"$1") | "empty"
			};
		}
		if findClear(page:url("query")) eq "1" then
		{
			notify("clearing", "removing any stored names...");
		}
		fired {
			clear ent:fname;
		}
	}

	rule show_form {
		select when pageview ".*" 
		pre {
			form = <<
<form id="nameForm" onsubmit="return false">
<span>First Name: </span><input name="fname">
<br>
<span>Last Name: </span><input name="lname">
<br>
<button id="submitBtn">Submit</button>
</form>
			>>;
		}
		if (not seen ".*" in ent:fname) then
		{
			append("#main", "<p>For some reason, I feel like these labs are just an extension of the KRL manual tutorials...</p>");
			append("#main", form);
			watch("#nameForm", "submit");
		}
		fired {
			last;
		}
	}
	rule clicked_rule {
        select when web submit "#nameForm"
		pre {
			first = page:param("fname");
     		last = page:param("lname");
		}
		notify("click happened", first);
    	fired {
    		set ent:fname first;
    		set ent:lname last;
    		raise explicit event got_name;
    	}
    }
	rule show_names {
		select when explicit got_name 
            or web pageview ".*" 
            pre {
            	name = ent:fname + " " + ent:lname;
            }
        {
	        replace_inner("#main", "<p>Name: #{name}</p>");
		}
	}
} 