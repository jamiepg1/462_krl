
ruleset lab2Ruleset {
	meta {
		name "cs 462 ruleset"
		description <<
			Written for cs462 w2014
		>>
		author "braveSirRobin"
		logging off
	}
	dispatch {
	}
	global {
	}
	rule rule1 is active {
		select when pageview ".*"
		{
			notify("Hello World", "This is a sample rule.");
			notify("notification 2", "This is another sample rule.");
		}
	}
		rule rule2 is active {
		select when pageview ".*"
			pre {
				findName = function(str) {
						(str.match(re#^.*?name=([^&]+).*#)) => str.replace(re#^.*?name=([^&]+).*#,"$1") | "Monkey"
				};
			}
		{
			notify("Greeter (rule2)", "Hello " + findName(page:url("query")));
		}
	}
	rule selfCounter {
		select when pageview ".*"
			if ent:selfCounterCount < 5 then
				notify("selfCounter", "Count is: " + ent:selfCounterCount);
		fired {
			ent:selfCounterCount += 1 from 1;
		} 
	}
	rule clearer {
		select when pageview ".*"
			pre {
				containsClearParameter = function(str) {
						str.match(re#^.*[&]clear=.*#) || str.match(re#^clear=.*#)
				};
			}
			if containsClearParameter(page:url("query")) then
					notify("clearer", "query string contains clear parameter. clearing selfCounterCount...");
			fired {
				clear ent:selfCounterCount;
			}
	}
} 